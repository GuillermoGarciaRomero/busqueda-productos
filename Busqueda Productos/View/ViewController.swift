//
//  ViewController.swift
//  Busqueda Productos
//
//  Created by Guillermo Garcia Romero on 11/23/18.
//  Copyright © 2018 Guillermo Garcia Romero. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UISearchBarDelegate,ServiceResultDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate {

    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    let tableViewCellIdentifier = "cell"
    let searchController = UISearchController(searchResultsController: nil)
    var dataSource:[ProductBO]?
    var page = 1
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func initView() {
        //Configuracion del UISearchController
        self.navigationItem.searchController = searchController
        searchController.searchBar.placeholder = "¿Qué buscas?"
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        }else{
            tableView.tableHeaderView = searchController.searchBar
        }
        //configuracion de la tabla
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        page = 1
        searchController.dismiss(animated: true, completion: nil)
        self.getProducts()
    }
    
    func getProducts(){
        let services = Services()
        services.delegate = self
        services.requestService(searchController.searchBar.text!,page: page)
        activityIndicator.startAnimating()
       
    }
    
    //MARK: - TableViewDelegateMethods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dataSource = dataSource {
            return dataSource.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier) as! ProductTableViewCell
        let producto = dataSource![indexPath.row]
        cell.lblName.text = producto.name
        cell.lblNormalPrice.text = "$\(producto.normalPrice)"
        cell.lblDiscountedPrice.text = "$\(producto.discountedPrice)"
        if let imageUrl = URL(string: producto.image){
            URLSession.shared.dataTask(with:imageUrl) { (data,_, _) in
                if let data = data{
                    DispatchQueue.main.async {
                        cell.iv.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
        
        return cell
    }
    
    //MARK: - ServiceResultDelegateMethods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y + scrollView.frame.height == scrollView.contentSize.height  {
//            page += 1
//            self.getProducts()
        }
    }
    
    //MARK: - ServiceResultDelegateMethods
    
    func didSuccessOnRequestForResult(result: Any) {
        
        dataSource = (result as! [ProductBO])
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    func didFailOnRequestForResult() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            
        }
    }

}


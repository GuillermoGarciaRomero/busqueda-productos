//
//  ProductTableViewCell.swift
//  Busqueda Productos
//
//  Created by Guillermo Garcia Romero on 11/23/18.
//  Copyright © 2018 Guillermo Garcia Romero. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNormalPrice: UILabel!
    @IBOutlet weak var lblDiscountedPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

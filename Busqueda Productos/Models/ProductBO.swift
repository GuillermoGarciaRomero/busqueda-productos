//
//  ProductBO.swift
//  Busqueda Productos
//
//  Created by Guillermo Garcia Romero on 11/23/18.
//  Copyright © 2018 Guillermo Garcia Romero. All rights reserved.
//

import UIKit


class ProductBO {
    var image : String
    var name : String
    var normalPrice: String
    var discountedPrice : String
    
    init(image:String,name:String,normalPrice:String,discountedPrice:String) {
        self.image = image
        self.name = name
        self.normalPrice = normalPrice
        self.discountedPrice = discountedPrice
    }
}

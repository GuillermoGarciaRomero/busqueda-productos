//
//  Services.swift
//  Busqueda Productos
//
//  Created by Guillermo Garcia Romero on 11/23/18.
//  Copyright © 2018 Guillermo Garcia Romero. All rights reserved.
//

import UIKit


protocol ServiceResultDelegate:NSObjectProtocol {
    func didSuccessOnRequestForResult(result:Any)
    func didFailOnRequestForResult()
}


class Services : NSObject {
    
    weak var delegate:ServiceResultDelegate?
    

    func requestService(_ productName:String, page:Int) {
        if let url = URL(string: "https://shoppapp.liverpool.com.mx/appclienteservices/services/v3/plp?search-string=\(productName)&page-number=\(page)") {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let session = URLSession(configuration: URLSessionConfiguration.default)
            let dataTask = session.dataTask(with: request, completionHandler: { (data,response,error) in
                if let _ = error {
                    print("Ocurrio un error")
                    if let _ = self.delegate{
                        self.delegate?.didFailOnRequestForResult()
                    }
                }else{
                    if let data = data{
                        do{
                            let json = try JSON(data: data)
                            if let productsArray = json["plpResults"]["records"].array {
                                var results:[ProductBO] = Array()
                                for product in productsArray {
                                    results.append(ProductBO.init(image: product["smImage"].stringValue,
                                                                  name: product["productDisplayName"].stringValue,
                                                                  normalPrice: product["listPrice"].stringValue,
                                                                  discountedPrice: product["promoPrice"].stringValue))
                                    
                                }
                                self.delegate?.didSuccessOnRequestForResult(result: results)
                            }
                        }catch{
                            print(error)
                            self.delegate?.didFailOnRequestForResult()
                        }
                       
                    }else{
                        print("La respuesta viene vacia")
                        self.delegate?.didFailOnRequestForResult()
                    }
                    
                    
                }
            })
            dataTask.resume()
        }
        
    }
    
}



